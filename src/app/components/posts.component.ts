import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.view.html',
})
export class PostsComponent implements OnInit {
  allPosts: any; // todo implement interface for this, rather than just any
  initialNumberPostsToDisplay: number;
  noMorePosts = false;
  posts = [];

  constructor(private http: HttpClient) { }

  fetchMockApi(): void {
    const endpoint = `https://jsonplaceholder.typicode.com/photos`;
    this.http.get(endpoint).subscribe(
      data => {
        this.allPosts = data;
        this.loadInitialPosts();
      }
    );
  }

  loadInitialPosts(): void {
    for (let i = 0; i < this.initialNumberPostsToDisplay; i++) {
      this.posts.push(this.allPosts[i]);
    }
    // remove posts that are already displayed
    this.allPosts.splice(0, this.initialNumberPostsToDisplay);
    console.log(`this.posts.length after loading initial: ${this.posts.length}`);
  }

  loadOneMorePost(): void {
    if (this.allPosts.length === 0) {
      this.noMorePosts = true;
    }

    this.posts.push(this.allPosts[0]);
    this.allPosts.shift();
  }

  ngOnInit(): void {
    this.fetchMockApi();
    this.initialNumberPostsToDisplay = 5;
  }
}
